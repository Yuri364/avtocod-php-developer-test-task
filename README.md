# Действия, которые необходимо выполнить для установки

Клонировать репозиторий 

git clone https://bitbucket.org/Yuri364/avtocod-php-developer-test-task.git

Установить зависимости

composer install

Создать файл окружения

cp .env.example .env

Применить миграции

artisan migrate --seed

Запустить

artisan serve --port 8080
