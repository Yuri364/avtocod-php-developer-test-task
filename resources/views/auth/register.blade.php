@extends('layouts.app', ['page_title' => 'Регистрация'])

@section('main_content')

<form class="form-signup" method="post" action="{{ route('register') }}">
	
	@csrf
	
	@if ($errors->has('login') || $errors->has('password'))
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
			&times;
		</button>
		@if ($errors->has('login'))
			<p>{{ $errors->first('login') }}</p>
		@endif
		@if ($errors->has('password'))
			<p>{{ $errors->first('password') }}</p>
		@endif
	</div>
	@endif
	
	<h2 class="form-signup-heading">Регистрация</h2>

	<label for="user_login" class="sr-only">Логин</label>
	<input type="text" id="user_login" name="login" class="form-control" placeholder="Логин" required autofocus>

	<label for="user_password" class="sr-only">Пароль</label>
	<input type="password" id="user_password" name="password" class="form-control" placeholder="Пароль" required>

	<label for="user_password_repeat" class="sr-only">Повторите пароль</label>
	<input type="password" id="user_password_repeat" name="password_confirmation" class="form-control" placeholder="Пароль (ещё раз)" required>

	<button class="btn btn-lg btn-primary btn-block" type="submit">
		Зарегистрироваться
	</button>
</form>

@endsection