@extends('layouts.app', ['page_title' => 'Авторизация'])

@section('main_content')

<form class="form-signin" method="post" action="{{ route('login') }}">
	
	@if ($errors->any())
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
			&times;
		</button>
		<strong>Ошибка!</strong> Вход в систему с указанными данными невозможен.
	</div>
	@endif
	
	@csrf

	<h2 class="form-signin-heading">Авторизация</h2>

	<label for="user_login" class="sr-only">Логин</label>
	<input type="text" id="user_login" name="login" class="form-control" placeholder="Логин" required autofocus>

	<label for="user_password" class="sr-only">Пароль</label>
	<input type="password" id="user_password" name="password" class="form-control" placeholder="Пароль" required>

	<div class="checkbox">
		<label>
			<input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
			Запомнить меня 
		</label>
	</div>
	<button class="btn btn-lg btn-primary btn-block" type="submit">
		Войти
	</button>
</form>

@endsection