<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Avtocod') }}</a>
        </div>
        
        <ul class="nav navbar-nav">
            <li{{( Request::is('/') ? ' class=active' : '') }}><a href="{{ url('/') }}">Главная</a></li>
            @guest
            	<li{{( Request::is('login') ? ' class=active' : '') }}><a href="{{ route('login') }}">Авторизация</a></li>
            	<li{{( Request::is('register') ? ' class=active' : '') }}><a href="{{ route('register') }}">Регистрация</a></li>
       		@endguest	
        </ul>
        
        @auth
        <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text"><span class="glyphicon glyphicon-user"></span> {{ Auth::user()->login }}</li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out"></span> Выход</a></li>
        </ul>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        	@csrf
        </form>
        @endauth
        
    </div>
</nav>