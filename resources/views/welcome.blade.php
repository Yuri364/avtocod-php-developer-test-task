@extends('layouts.app', ['page_title' => 'Стена сообщений'])

@section('html_header')

@endsection

@section('main_content')

<div class="page-header">
        <h1>Сообщения от всех пользователей</h1>
    </div>

	@auth
    <form action="{{ route('message') }}" method="post" class="form-horizontal">
    	
    	@csrf
    	@if($errors->has('message'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Ошибка!</strong> {{ $errors->first('message') }}
        </div>
		@endif
				
        <div class="controls">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="message_text">Текст сообщения:</label>
                    <textarea id="message_text" name="message" class="form-control" placeholder="Ваше сообщение" rows="4"></textarea>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <input type="submit" class="btn btn-success btn-send" value="Отправить сообщение" />
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
    @endauth
   
	@if (count($messages))
		@foreach ($messages as $msg)
			
		    <div class="row wall-message">
		    	@auth
			    	@if((Auth::user()->id == $msg->user->id) || (Auth::user()->is_admin))
			    		<a href="{{ action('MessageController@destroy', ['id' => $msg->id]) }}" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>
			        @endif
		        @endauth
		        <div class="col-md-1 col-xs-2">
		            <img src="{{ Gravatar::get('chudinovyuri@gmail.com', ['size'=>200]) }}" alt="" class="img-circle user-avatar" />
		        </div>
		        <div class="col-md-11 col-xs-10">
		            <p>
		                <strong>{{ $msg->user->login }}:</strong>
		            </p>
		            <blockquote>
		                {{ $msg->message }}
		            </blockquote>
		        </div>
		    </div>
    	@endforeach
    @else
    
    <h3>Ничего не найдено!</h3>
     
    @endif


@endsection
