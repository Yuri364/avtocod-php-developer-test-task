<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\AbstractTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends AbstractTestCase
{
	use RefreshDatabase;
	
    /**
     * Новый пользователь
	 * 
     * @return void
     */
    public function testCreateUser()
    {
        factory(User::class)->create([
            'login' => 'user123',
        ]);
		
		//Таблица в базе данных содержит данные
        $this->assertDatabaseHas('users', [
            'login' => 'user123'
        ]);
    }
	
	/**
     * Обновить пользователя
	 * 
     * @return void
     */
    public function testEditUser()
    {
        factory(User::class)->create([
            'login' => 'user123',
        ]);
		
        User::where('login', 'user123')->update(['login' => 'newlogin']);
		
		//Таблица в базе данных не содержит данные
        $this->assertDatabaseMissing('users', [
            'login' => 'user123'
        ]);
		
		//Таблица в базе данных содержит данные
        $this->assertDatabaseHas('users', [
            'login' => 'newlogin'
        ]);
    }
	
	/**
     * Удалить пользователя
     *
     * @return void
     */
    public function testDeleteUser()
    {
        factory(User::class)->create([
            'login' => 'user123',
        ]);
		
        User::where('login', 'user123')->delete();
		
        //Таблица в базе данных не содержит данные
        $this->assertDatabaseMissing('users', [
            'login' => 'user123'
        ]);
    }
	
}
