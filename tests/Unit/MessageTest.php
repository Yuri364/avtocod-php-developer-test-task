<?php

namespace Tests\Unit;

use Tests\AbstractTestCase;
use App\Models\User;
use App\Models\Message;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MessageTest extends AbstractTestCase
{
	
	use RefreshDatabase;
	
	/**
     * Новое сообщение
     *
     * @return void
     */
    public function testCreateMessage()
    {
        factory(User::class)
            ->create([
                'login' => 'login123'
            ])
            ->each(function ($user) {
                $user->messages()->save(factory(Message::class)
                			     ->make(['message' => 'message']));
            });
			
        $user = User::first();
		
		//Таблица в базе данных содержит данные
        $this->assertDatabaseHas('messages', [
            'message' => 'message',
            'user_id' => $user->id
        ]);
    }
	
	/**
     * Удалить сообщение
     *
     * @return void
     */
    public function testDeleteMessage()
    {
        factory(User::class)
            ->create([
                'login' => 'login123'
            ])
            ->each(function ($user) {
                $user->messages()->save(factory(Message::class)
                			     ->make(['message' => 'message']));
            });
			
        $message = Message::first();
		Message::where('id', $message->id)->delete();
		
		//Таблица в базе данных не содержит данные
        $this->assertDatabaseMissing('messages', [
            'id' => $message->id
        ]);
    }
    
}
