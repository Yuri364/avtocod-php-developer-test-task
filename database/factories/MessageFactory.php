<?php

use App\Models\Message;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

$factory->define(\App\Models\Message::class, function (Faker $faker) {
    return [
        'message' => $faker->text,
    ];
});
