<?php

use App\Models\User;
use App\Models\Message;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->times(\mt_rand(2, 5))
        					->create()
        					->each(function (User $user) {
            					$user->messages()
            					->saveMany(factory(Message::class, rand(5, 7))
            					->make());
        					});
    }
}
