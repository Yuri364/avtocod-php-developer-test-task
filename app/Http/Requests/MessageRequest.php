<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Разрешено ли пользователю выполнить этот запрос.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Валидация
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|min:3'
        ];
    }
}
