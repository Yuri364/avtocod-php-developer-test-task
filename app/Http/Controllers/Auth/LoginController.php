<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Авторизация пользователей
    |--------------------------------------------------------------------------
    */

    use AuthenticatesUsers;

    /**
     * Куда перенаправить после авторизации
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	/**
     * Имя пользователя для входа, которое будет использоваться контроллером
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }
}
