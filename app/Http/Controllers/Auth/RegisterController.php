<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Регистрация пользователей
    |--------------------------------------------------------------------------
    */

    use RegistersUsers;

    /**
     * Куда перенаправить после регистрации.
     *
     * @var string
     */
    protected $redirectTo = '/register/success';

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Валидация введенных данных
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'login' => 'required|string|min:8|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Создание нового пользователя
     *
     * @param  array  $data
     *
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'login' => $data['login'],
            'password' => Hash::make($data['password']),
        ]);
    }
	
	
	/**
     * Регистрация
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function register(Request $request)
     {
     	$this->validator($request->all())->validate();
		
     	event(new Registered($user = $this->create($request->all())));
		
     	return $this->registered($request, $user) 
     							 ?: redirect($this->redirectPath());
     }
	
	/**
	 * Страница успешной регистрации
	 * 
	 * @return \Illuminate\Http\Response
	 */
	
	public function success()
	{
		return view('auth.reg_success');
	}
	
	
}
