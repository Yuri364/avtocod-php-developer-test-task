<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;

class WelcomeController extends Controller
{
	/**
	 * Все сообщения
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $messages = Message::with('user')
        			->orderBy('created_at', 'desc')
        			->get();
					
		return view('welcome', ['messages' => $messages]);
		
    }
}
