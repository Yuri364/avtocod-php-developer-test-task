<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Http\Requests\MessageRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
	
	 /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	
    /**
     * Добавить запись в базу
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageRequest $request)
    {
        $message = new Message;
		$message->message = $request->message;
		$message->user_id = Auth::user()->getAuthIdentifier();
		
		$message->save();
		return redirect('/');
    }

    /**
     * Удаление записи
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = Message::find($id);
		$user = Auth::user();
		
		if(($message && $message->user_id == $user->id) 
			|| ($message && $user->is_admin)) {
			Message::destroy($id);
			return redirect('/');
		}	
    }
}
