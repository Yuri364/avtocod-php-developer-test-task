<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'message'
    ];

    /**
     * Атрибуты, которые должны быть невидимы для массива.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];
	
	public function user()
    {
        return $this->belongsTo(User::class);
    }
	
}
