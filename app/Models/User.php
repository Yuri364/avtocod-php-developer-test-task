<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'password',
    ];

    /**
     * Атрибуты, которые должны быть невидимы для массива.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
	
	public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
