<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Авторизация
Auth::routes();

//Главная страница
Route::get('/', 'WelcomeController@index')->name('index');

//Успешная регистрация
Route::get('/register/success', 'Auth\RegisterController@success');

//Новое сообщение
Route::post('/message', 'MessageController@store')->name('message');

//Удалить сообщение
Route::get('/message/destroy/{id}', 'MessageController@destroy');